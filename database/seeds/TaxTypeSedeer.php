<?php

use Illuminate\Database\Seeder;

class TaxTypeSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entry = new \App\Models\TaxType();
        $entry->title = 'N/A';
        $entry->tax_percentage = 0;
        $entry->save();

        $entry = new \App\Models\TaxType();
        $entry->title = 'VAT';
        $entry->tax_percentage = 15;
        $entry->save();
    }
}
