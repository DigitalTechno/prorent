<?php

use Illuminate\Database\Seeder;

class PropertyTypeSeeder extends Seeder
{
    /**`
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new \App\Models\PropertyType();
        $category->title = 'For Sale';
        $category->save();

        $category = new \App\Models\PropertyType();
        $category->title = 'To Rent';
        $category->save();

    }
}
