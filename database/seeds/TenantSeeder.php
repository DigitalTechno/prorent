<?php

use Illuminate\Database\Seeder;

class TenantSeeder extends Seeder
{
    /**`
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10000; $i++) {
            $user = new \App\User();
            $user->name = 'test';
            $user->surname = 'tenant' . $i;
            $user->user_type_id = 3;
            $user->email = 'testtenant' . $i . '@gmail.com';
            $user->contact_number = '073 455 78'.$i;
            $user->password = bcrypt('test');
            $user->save();

        }

        $user = new \App\User();
        $user->name = 'Admin';
        $user->surname = 'ProRental';
        $user->user_type_id = 1;
        $user->email = 'admin@prorental.com';
        $user->contact_number = '073 455 7809';
        $user->password = bcrypt('admin');
        $user->save();

        for($i=0; $i<20; $i++) {
            $user = new \App\User();
            $user->name = 'Admin';
            $user->surname = 'ProRental' . $i;
            $user->user_type_id = 1;
            $user->email = 'admin@prorental.com'.$i;
            $user->contact_number = '073 455 78'.$i;
            $user->password = bcrypt('admin');
            $user->save();
        }
    }
}
