<?php

use Illuminate\Database\Seeder;

class RecurringSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entry = new \App\Models\Recurring();
        $entry->title = 'No';
        $entry->save();

        $entry = new \App\Models\Recurring();
        $entry->title = 'Yes';
        $entry->save();
    }
}
