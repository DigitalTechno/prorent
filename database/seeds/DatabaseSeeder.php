<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PaymentStatusSedeer::class);
        $this->call(RecurringCycleSedeer::class);
        $this->call(RecurringSedeer::class);
        $this->call(TaxTypeSedeer::class);
        $this->call(CategorySeeder::class);
        $this->call(PropertyTypeSeeder::class);
        $this->call(ProvinceSeeder::class);
    }
}
