<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**`
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $province = new \App\Models\Province();
        $province->title = 'Western Cape';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Eastern Cape';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Northern Cape';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Limpopo';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'North West';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Gauteng';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Kwazulu Natal';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Mpumalanga';
        $province->save();

        $province = new \App\Models\Province();
        $province->title = 'Free State';
        $province->save();

    }
}
