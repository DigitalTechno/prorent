<?php

use Illuminate\Database\Seeder;

class RecurringCycleSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entry = new \App\Models\RecurringCycle();
        $entry->title = 'Monthly';
        $entry->save();

        $entry = new \App\Models\RecurringCycle();
        $entry->title = 'Quarterly';
        $entry->save();

        $entry = new \App\Models\RecurringCycle();
        $entry->title = 'Semi Annually';
        $entry->save();

        $entry = new \App\Models\RecurringCycle();
        $entry->title = 'Annually';
        $entry->save();
    }
}
