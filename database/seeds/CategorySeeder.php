<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**`
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new \App\Models\Category();
        $category->title = 'House';
        $category->save();

        $category = new \App\Models\Category();
        $category->title = 'Apartment / Flat';
        $category->save();

        $category = new \App\Models\Category();
        $category->title = 'Townhouse';
        $category->save();

        $category = new \App\Models\Category();
        $category->title = 'Farm';
        $category->save();

        $category = new \App\Models\Category();
        $category->title = 'Commercial Property';
        $category->save();

        $category = new \App\Models\Category();
        $category->title = 'Industrial Property';
        $category->save();

    }
}
