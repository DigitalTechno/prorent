<?php

use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i<10000; $i++) {
            $user = new \App\User();
            $user->name = 'test'.$i;
            $user->surname = 'Agent'.$i;
            $user->user_type_id = 2;
            $user->email = 'testagent'.$i.'@gmail.com';
            $user->contact_number = '073 450 8'.$i;
            $user->password = bcrypt('test');
            $user->save();
        }
    }
}
