<?php

use Illuminate\Database\Seeder;

class PaymentStatusSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $province = new \App\Models\PaymentStatus();
        $province->title = 'UNPAID';
        $province->save();

        $province = new \App\Models\PaymentStatus();
        $province->title = 'PARTIALLY PAID';
        $province->save();

        $province = new \App\Models\PaymentStatus();
        $province->title = 'PAID ';
        $province->save();

        $province = new \App\Models\PaymentStatus();
        $province->title = 'OVERDUE ';
        $province->save();
    }
}
