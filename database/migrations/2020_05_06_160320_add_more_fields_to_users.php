<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('date_of_birth')->nullable();
            $table->string('marital_status')->nullable();
            $table->integer('gender')->nullable();
            $table->string('work_address')->nullable();
            $table->renameColumn('address', 'home_address');
            $table->integer('province_id')->nullable();
            $table->string('city')->nullable();
            $table->string('suburb')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['date_of_birth', 'mairital_status', 'gender', 'work_address', 'home_address', 'province_id', 'city', 'suburb']);
        });
    }
}
