(function($) {
    "use strict";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Forms
    $(document).on('submit', '.ajax-form', function (e) {
        e.preventDefault();

        // Get submit button
        var thisButton = $(this).find('button[type="submit"]');

        // Get button text (current)
        var thisButtonText = thisButton.html();

        // Change button html
        thisButton.html('Submitting...');

        // Disable button
        thisButton.prop('disabled', true);

        // Get form data
        var formData = new FormData($(this)[0]);

        // Do ajax post
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType:'json',
            async:false,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function (response) {
                // do nothing
            },
            success: function (response) {

                if (response.success) {
                    PNotify.success({
                        text: response.success
                    });

                    // Restore text
                    thisButton.html(thisButtonText);

                    // Re-enable button
                    thisButton.prop('disabled', false);

                    if (response.invoiceId) {
                        setTimeout(function () {
                            window.location.href = window.origin+"/invoice/info/"+response.invoiceId;
                        }, 2000);
                    }

                    if (response.refresh) {
                        setTimeout(function () {
                            location.reload(); //reload page after 2 sec.
                        }, 2000);
                    } else if (response.disable) {
                        thisButton.prop('disabled', true);
                    }
                }
            },
            error: function (response) {
                if (response.status == 422) {
                    // Start off by hiding the errors
                    $('span.error').html('');

                    // Process Validation Errors here
                    var validationErrors = response.responseJSON.errors;

                    // Loop through errors
                    // TODO: Refactor
                    for (var key in validationErrors) {
                        if ($("[name='" + key + "']").length > 0) {
                            // Find element by name
                            $("[name='" + key + "']").parent().find('span.error').html(validationErrors[key][0]);
                        } else {
                            // Find element by class name (stop-gap solution put in place for array elements)
                            $('.' + key).parent().find('span.error').html(validationErrors[key][0]);
                        }
                    }
                }

                // Restore text
                thisButton.html(thisButtonText);

                // Re-enable button
                thisButton.prop('disabled', false);

            }
        });
    });

    // upload user profile
    $(document).off('click', '.add-invoice-item');
    $(document).on('click', '.add-invoice-item', function(e) {
        e.preventDefault();
        var invoice_id = $(this).attr('data-id');

        $.ajax({
            url: `/invoice/${invoice_id}/add/item`,
            type: 'GET',
            data: {},
            processData: false,
            contentType: false,
            success: function(response) {
                $('.invoice-items').html(response.html);
                $.init();
            },
            error: function(error) {
                console.log(error)
            }
        })
    });

})(jQuery);
