$(document).ready(function(){
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});

    var oTable = $('#admin-users').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/users/admin',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                //  set filter values
                d.user_id = $('input[name="user_id"]').val();
                sessionStorage.setItem('filters' , JSON.stringify(d));
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],

        "columns" : [
            {data : 'name', searchable: true},
            {data : 'surname', orderable: true, searchable: true},
            {data : 'email', orderable: true, searchable: true},
            {data : 'contact_number', searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#agents').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/users/agents',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                //  set filter values
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],

        "columns" : [
            {data : 'name', searchable: true},
            {data : 'surname', orderable: true, searchable: true},
            {data : 'email', orderable: true, searchable: true},
            {data : 'contact_number', searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#tenants').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/users/tenants',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'name', searchable: true},
            {data : 'surname', orderable: true, searchable: true},
            {data : 'email', orderable: true, searchable: true},
            {data : 'contact_number', searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#propertiesTable').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/properties',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'title', searchable: true},
            {data : 'category', orderable: true, searchable: false},
            {data : 'property_type', orderable: true, searchable: false},
            {data : 'description', orderable: true, searchable: true},
            {data : 'price', orderable: true, searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#agentProperties').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/agent/properties',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                d.agent_id = $('input[name="user_id"]').val();
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'title', searchable: true},
            {data : 'category', orderable: true, searchable: false},
            {data : 'property_type', orderable: true, searchable: false},
            {data : 'description', orderable: true, searchable: true},
            {data : 'price', orderable: true, searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#agentTenants').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/agent/tenants',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                d.agent_id = $('input[name="user_id"]').val();
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'name', searchable: true},
            {data : 'surname', orderable: true, searchable: true},
            {data : 'email', orderable: true, searchable: true},
            {data : 'contact_number', searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#tenantProperties').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/tenant/properties',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                d.tenant_id = $('input[name="user_id"]').val();
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'title', searchable: true},
            {data : 'category', orderable: true, searchable: false},
            {data : 'property_type', orderable: true, searchable: false},
            {data : 'description', orderable: true, searchable: true},
            {data : 'price', orderable: true, searchable: true},
            {data : 'created_at', searchable: true},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });

    var oTable = $('#adminInvoices').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/admin/invoices',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'invoice_number', searchable: true},
            {data : 'status', orderable: true, searchable: false},
            {data : 'tenant', orderable: true, searchable: false},
            {data : 'due_date', orderable: true, searchable: true},
            {data: 'amount_due', searchable: false},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });
    var oTable = $('#agentInvoices').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/agent/invoices',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                d.agent_id = $('input[name="user_id"]').val();
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'invoice_number', searchable: true},
            {data : 'status', orderable: true, searchable: false},
            {data : 'tenant', orderable: true, searchable: false},
            {data : 'due_date', orderable: true, searchable: true},
            {data: 'amount_due', searchable: false},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });
    var oTable = $('#tenantInvoices').DataTable({
        destroy : true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        paging : true,
        "ajax" : {
            url : '/api/tenant/invoices',
            dataSrc : 'data',
            beforeSend : function (){
                $('.pre-loader').removeClass('hide');
            },
            data : function (d , c) {
                d.tenant_id = $('input[name="user_id"]').val();
            },
            complete : function (response) {
                $('.pre-loader').addClass('hide');
            }
        },
        "lengthMenu": [[ 20, 30, 50],[ 20, 30, 50]],
        // "dom": '<Blf<t>ip>',
        // "buttons": [
        //     { extend: 'excel', exportOptions: { columns: ':visible', rows: ':visible' }, className: 'btn m-l-15 btn-inverse',  text: 'Export to Excel' },
        // ],


        "columns" : [
            {data : 'invoice_number', searchable: true},
            {data : 'status', orderable: true, searchable: false},
            {data : 'tenant', orderable: true, searchable: false},
            {data : 'due_date', orderable: true, searchable: true},
            {data: 'amount_due', searchable: false},
            {data: 'actions', orderable: false}
        ],
        "columnDefs": [ {

            "targets": 1,
            "data": "data",
            "orderable" : false
        },
            {
                "targets": 2,
                "orderable" : false
            }
        ]
    });
});
