<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/users/admin', 'DatatableController@adminUsers');
Route::get('/users/agents', 'DatatableController@agents');
Route::get('/users/tenants', 'DatatableController@tenants');
Route::get('/agent/tenants', 'DatatableController@agentTenants');

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/properties', 'DatatableController@properties');
Route::get('/agent/properties', 'DatatableController@agentProperties');
Route::get('/tenant/properties', 'DatatableController@tenantProperties');

/*
|--------------------------------------------------------------------------
| Invoices Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/admin/invoices', 'DatatableController@adminInvoices');
Route::get('/agent/invoices', 'DatatableController@agentInvoices');
Route::get('/tenant/invoices', 'DatatableController@tenantInvoices');