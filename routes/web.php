<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/', 'AdminController@index')->name('index')->middleware('auth');
Route::get('/custom/login/{id}', 'CustomAuthController@replaceAuth')->name('custom.login')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/users/admins', 'UserController@admins')->name('admins')->middleware('auth');
Route::get('/users/agents', 'UserController@agents')->name('agents')->middleware('auth');
Route::get('/users/tenants', 'UserController@tenants')->name('tenants')->middleware('auth');
Route::post('/users/store', 'UserController@store')->name('users.store')->middleware('auth');
Route::get('/users/edit/{id}', 'UserController@edit')->name('users.edit')->middleware('auth');
Route::get('/users/profile/{id}', 'UserController@info')->name('users.info')->middleware('auth');
Route::get('/users/delete/{id}', 'UserController@delete')->name('users.delete')->middleware('auth');
Route::post('/users/update/{id}', 'UserController@update')->name('users.update')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Properties Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/properties', 'PropertyController@index')->name('properties.index')->middleware('auth');
Route::post('/properties/store', 'PropertyController@store')->name('properties.store')->middleware('auth');
Route::get('/properties/edit/{id}', 'PropertyController@edit')->name('properties.edit')->middleware('auth');
Route::get('/properties/info/{id}', 'PropertyController@info')->name('properties.info')->middleware('auth');
Route::get('/properties/delete/{id}', 'PropertyController@delete')->name('properties.delete')->middleware('auth');
Route::post('/properties/update/{id}', 'PropertyController@update')->name('properties.update')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Agent Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/agent/properties', 'PropertyController@agent_properties')->name('agent.properties')->middleware('auth');
Route::get('/agent/tenants', 'UserController@agent_tenants')->name('agent.tenants')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Tenants Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/tenant/properties', 'PropertyController@tenant_properties')->name('tenant.properties')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Invoices Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/agent/invoices', 'InvoiceController@agent_invoices')->name('agent.invoices')->middleware('auth');
Route::get('/tenant/invoices', 'InvoiceController@tenant_invoices')->name('tenant.invoices')->middleware('auth');
Route::get('/admin/invoices', 'InvoiceController@admin_invoices')->name('admin.invoices')->middleware('auth');
Route::post('/invoice/store', 'InvoiceController@store')->name('store.invoice')->middleware('auth');
Route::get('/invoice/info/{invoice_id}', 'InvoiceController@info')->name('invoice.info')->middleware('auth');
Route::get('/invoice/delete/{invoice_id}', 'InvoiceController@delete')->name('invoice.delete')->middleware('auth');
Route::get('/invoice/send/{invoice_id}', 'InvoiceController@send')->name('invoice.send')->middleware('auth');
Route::get('/invoice/edit/{invoice_id}', 'InvoiceController@edit')->name('invoice.edit')->middleware('auth');
Route::post('/invoice/update/{invoice_id}', 'InvoiceController@update')->name('invoice.update')->middleware('auth');
Route::get('/invoice/{invoice_id}/add/item', 'InvoiceController@addItem')->name('invoice.add.item')->middleware('auth');
Route::get('/invoice/download/{invoice_id}', 'InvoiceController@download')->name('invoice.download')->middleware('auth');

/*
|--------------------------------------------------------------------------
| Payments Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/payment/success', 'PaymentController@success')->name('payment.success')->middleware('auth');
Route::get('/payment/cancel', 'PaymentController@cancel')->name('payment.cancel')->middleware('auth');
Route::match(['get','post','put'], '/itn' , 'PaymentController@itn')->name('itn');

