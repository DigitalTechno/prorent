<?php

namespace App\Repositories\Properties;

use DB;
use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyType;

class PropertyRepository
{
    public function getProperties()
    {
        return Property::select('properties.*',
            'categories.title as category',
            'property_types.title as property_type')
            ->join('categories', 'categories.id', '=', 'properties.category_id')
            ->join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->orderBy('properties.title', 'asc');
    }

    public function getAgentProperties($agent_id)
    {
        return Property::select('properties.*',
            'categories.title as category',
            'property_types.title as property_type')
            ->join('categories', 'categories.id', '=', 'properties.category_id')
            ->join('agent_properties', 'agent_properties.property_id', '=', 'properties.id')
            ->join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->where('agent_properties.agent_id', $agent_id)
            ->orderBy('properties.title', 'asc');
    }

    public function getTenantProperties($tenant_id)
    {
        return Property::select('properties.*',
            'categories.title as category',
            'property_types.title as property_type')
            ->join('categories', 'categories.id', '=', 'properties.category_id')
            ->join('tenant_properties', 'tenant_properties.property_id', '=', 'properties.id')
            ->join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->where('tenant_properties.tenant_id', $tenant_id)
            ->orderBy('properties.title', 'asc');
    }

    public function getPropertyCategories()
    {
        return Category::orderBy('title', 'asc')->get();
    }

    public function getPropertyTypes()
    {
        return PropertyType::orderBy('title', 'asc')->get();
    }

    public function getPropertyById($decodedId)
    {
        return Property::select('properties.*',
            DB::raw('CONCAT(users.name, " ", users.surname) as tenant'),
            'categories.title as category', 'provinces.title as province',
            'property_types.title as property_type', 'tenant_properties.tenant_id')
            ->join('categories', 'categories.id', '=', 'properties.category_id')
            ->join('property_types', 'property_types.id', '=', 'properties.property_type_id')
            ->leftJoin('tenant_properties', 'tenant_properties.property_id', '=', 'properties.id')
            ->leftJoin('provinces', 'properties.province_id', '=', 'provinces.id')
            ->leftJoin('users', 'users.id', '=', 'tenant_properties.tenant_id')
            ->where('properties.id', $decodedId)
            ->first();
    }
}