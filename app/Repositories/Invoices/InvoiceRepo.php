<?php

namespace App\Repositories\Invoices;

use App\Models\InvoiceItem;
use DB;
use App\Models\Invoice;

class InvoiceRepo
{
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function getAdminInvoices()
    {
        return $this->invoice->select('invoices.*', 'properties.title', 'payment_statuses.title as status',
            DB::raw('CONCAT(users.name , " ", users.surname) as tenant'),
            DB::raw('SUM(invoice_items.credit) as total_credit'),
            DB::raw('SUM(invoice_items.debit) as total_debit'))
            ->join('users', 'users.id', '=', 'invoices.tenant_id')
            ->join('properties', 'properties.id', '=', 'invoices.property_id')
            ->leftjoin('invoice_items', 'invoice_items.invoice_id', '=', 'invoices.id')
            ->leftJoin('payment_statuses', 'payment_statuses.id', '=', 'invoices.payment_status_id')
            ->groupBy('invoices.id');
    }

    public function getAgentInvoices($agentId)
    {
        return $this->invoice->select('invoices.*', 'properties.title', 'payment_statuses.title as status',
            DB::raw('CONCAT(users.name , " ", users.surname) as tenant'),
            DB::raw('SUM(invoice_items.credit) as total_credit'),
            DB::raw('SUM(invoice_items.debit) as total_debit'))
            ->join('users', 'users.id', '=', 'invoices.tenant_id')
            ->join('properties', 'properties.id', '=', 'invoices.property_id')
            ->leftjoin('invoice_items', 'invoice_items.invoice_id', '=', 'invoices.id')
            ->join('agent_properties', 'agent_properties.property_id', '=', 'properties.id')
            ->leftJoin('payment_statuses', 'payment_statuses.id', '=', 'invoices.payment_status_id')
            ->where('agent_properties.agent_id', $agentId)
            ->groupBy('invoices.id');
    }

    public function getTenantInvoices($tenantId)
    {
        return $this->invoice->select('invoices.*', 'properties.title',
            'payment_statuses.title as status', 'users.email',
            'users.contact_number', 'users.home_address',
            DB::raw('CONCAT(users.name , " ", users.surname) as tenant'),
            DB::raw('SUM(invoice_items.credit) as total_credit'),
            DB::raw('SUM(invoice_items.debit) as total_debit'))
            ->join('users', 'users.id', '=', 'invoices.tenant_id')
            ->join('properties', 'properties.id', '=', 'invoices.property_id')
            ->leftjoin('invoice_items', 'invoice_items.invoice_id', '=', 'invoices.id')
            ->join('tenant_properties', 'tenant_properties.property_id', '=', 'properties.id')
            ->leftJoin('payment_statuses', 'payment_statuses.id', '=', 'invoices.payment_status_id')
            ->where('tenant_properties.tenant_id', $tenantId)
            ->groupBy('invoices.id');
    }

    public function getInvoice($invoiceId)
    {
        return $this->invoice->select('invoices.*', 'properties.title', 'payment_statuses.title as status',
            DB::raw('CONCAT(users.name , " ", users.surname) as tenant'))
            ->join('users', 'users.id', '=', 'invoices.tenant_id')
            ->join('properties', 'properties.id', '=', 'invoices.property_id')
            ->leftJoin('payment_statuses', 'payment_statuses.id', '=', 'invoices.payment_status_id')
            ->where('invoices.id', $invoiceId)
            ->first();
    }

    public function getInvoiceItems($invoiceId)
    {
        $items = InvoiceItem::where('invoice_id', $invoiceId)->count();
        if(!$items){
            InvoiceItem::insert([
                'invoice_id' => $invoiceId,
                'title' => 'No Description'
            ]);
        }
        return InvoiceItem::where('invoice_id', $invoiceId)->get();
    }

    public function addInvoiceItem($invoiceId)
    {
        return InvoiceItem::insert([
                'invoice_id' => $invoiceId,
                'title' => 'No Description'
            ]);
    }
}