<?php

namespace App\Repositories\Users;

use App\User;


class UserRepository
{
    public function getAdminUsers()
    {
        return User::where('user_type_id', 1);
    }

    public function getAgents()
    {
        return User::where('user_type_id', 2);
    }

    public function getTenants()
    {
        return User::where('user_type_id', 3);
    }

    public function getAgentTenants($agent_id)
    {
        return User::select('users.*')
            ->join('agent_users', 'agent_users.user_id', '=', 'users.id')
            ->where(['users.user_type_id' => 3, 'agent_users.agent_id' => $agent_id]);
    }

    public function getUserById($userId)
    {
        return User::select('users.*', 'provinces.title as province', 'files.path')
            ->leftJoin('provinces', 'provinces.id', '=', 'users.province_id')
            ->leftJoin('files', 'files.id', '=', 'users.avatar')
            ->where('users.id', $userId)
            ->first();
    }
}