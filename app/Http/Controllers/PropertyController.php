<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyType;
use App\Models\Province;
use App\User;
use Illuminate\Http\Request;
use App\Services\Properties\PropertyService;
use App\Repositories\Properties\PropertyRepository;

class PropertyController extends Controller
{
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    /**
     * @var PropertyService
     */
    private $propertyService;

    public function __construct(
        PropertyRepository $propertyRepository,
        PropertyService $propertyService
    )
    {
        $this->propertyRepository = $propertyRepository;
        $this->propertyService = $propertyService;
    }

    public function index()
    {
        $categories = $this->propertyRepository->getPropertyCategories();
        $property_types = $this->propertyRepository->getPropertyTypes();
        return view('admin.properties.index', compact('categories', 'property_types'));
    }

    public function store(Request $request)
    {
        $this->propertyService->storeProperty($request);
        return response()->json([
            'success' => 'Property saved successfully.',
            'refresh' => true
        ]);
    }

    public function edit($id)
    {
        $property_id = decodeId($id);
        $property = Property::find($property_id);
        return view('admin.properties.edit', compact('property'));
    }

    public function info($id)
    {
        $decodedId = decodeId($id);
        $provinces = Province::orderBy('title', 'asc')->get();
        $property_types = PropertyType::orderBy('title', 'asc')->get();
        $categories = Category::orderBy('title', 'asc')->get();
        $tenants = User::where('user_type_id', 3)->orderBy('name', 'asc')->limit('20')->get();
        $property = $this->propertyRepository->getPropertyById($decodedId);
        return view('admin.properties.info', compact(
            'property',
            'provinces',
            'property_types',
            'categories',
            'tenants'
        ));
    }

    public function update(Request $request, $encodedId)
    {
        $this->propertyService->update($request, $encodedId);
        return response()->json([
            'success' => 'Property updated successfully.',
            'refresh' => true
        ]);
    }

    public function agent_properties()
    {
        $categories = $this->propertyRepository->getPropertyCategories();
        $property_types = $this->propertyRepository->getPropertyTypes();
        return view('agents.properties.index', compact('categories', 'property_types')) ;
    }

    public function tenant_properties()
    {
        return view('tenants.properties.index') ;
    }

    public function delete($id)
    {
        $property_id = decodeId($id);
        $property = Property::find($property_id);
        $property->delete();
        return redirect()->back();
    }

}
