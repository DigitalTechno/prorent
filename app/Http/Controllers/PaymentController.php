<?php

namespace App\Http\Controllers;

use App\Models\InvoiceItem;
use Billow\Payfast;
use DB;
use App\Repositories\Invoices\InvoiceRepo;
use App\Models\Invoice;
use App\Services\Invoices\InvoiceService;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $payfast;
    protected $invoiceService;
    protected $invoiceRepository;

    public function __construct(
        Payfast $payfast,
        InvoiceService $invoiceService,
        InvoiceRepo $invoiceRepository
    )
    {
        $this->payfast = $payfast;
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function success()
    {
        dd('success');
    }

    public function cancel()
    {
        dd('test');
    }

    public function itn(Request $request)
    {
        DB::beginTransaction();

        try{

            // get invoice attempted to be purchased
            $invoice =  Invoice::where('invoice_number', $request->m_payment_id)->first();

            $total_credit = 0;
            $total_debit = 0;

            $invoice_items = $this->invoiceRepository->getInvoiceItems($invoice->id);

            foreach ($invoice_items as $item){
                $total_credit += $item->credit;
                $total_debit += $item->debit;
            }

            $total_due = $total_debit-$total_credit;

            $status = $this->payfast->verify($request, $total_due)->status();

            // Handle the result of the transaction.
            switch( $status )
            {
                case 'COMPLETE':
                    $invoice_item = new InvoiceItem();
                    $invoice_item->credit = $total_due;
                    $invoice_item->ref = $invoice->invoice_number;
                    $invoice_item->title = 'EFT Payment';
                    $invoice_item->invoice_id = $invoice->id;
                    $invoice_item->date = now();
                    $invoice_item->save();

                    $invoice->payment_status_id = 3;
                    $invoice->save();
                    break;
                case 'FAILED':
                    break;
                case 'PENDING':
                    break;
                default:
                    break;
            }

            DB::commit();
            return $status;

        }
        catch (\Exception $e){
            DB::rollBack();
//            Log::error($e->getMessage());
        }
    }
}
