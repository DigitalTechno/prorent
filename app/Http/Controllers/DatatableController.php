<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Users\UserRepository;
use App\Repositories\Invoices\InvoiceRepo;
use App\Repositories\Properties\PropertyRepository;

class DatatableController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var InvoiceRepo
     */
    private $invoiceRepository;

    public function __construct(
        UserRepository $userRepository,
        PropertyRepository $propertyRepository,
        InvoiceRepo $invoiceRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->propertyRepository = $propertyRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function adminUsers()
    {
        $users = $this->userRepository->getAdminUsers();
        return datatables()->of($users)
            ->addColumn('actions' , function ($user) {
                return \View::make('admin.users.partials.actions' , [
                    'user_id' => $user->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function agents()
    {
        $users = $this->userRepository->getAgents();
        return datatables()->of($users)
            ->addColumn('actions' , function ($user) {
                return \View::make('admin.users.partials.actions' , [
                    'user_id' => $user->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function tenants()
    {
        $users = $this->userRepository->getTenants();
        return datatables()->of($users)
            ->addColumn('actions' , function ($user) {
                return \View::make('admin.users.partials.actions' , [
                    'user_id' => $user->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function properties(Request $request)
    {
        $properties = $this->propertyRepository->getProperties();
        return datatables()->of($properties)
            ->addColumn('actions' , function ($property) {
                return \View::make('admin.properties.partials.actions' , [
                    'property_id' => $property->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function agentProperties(Request $request)
    {
        $properties = $this->propertyRepository->getAgentProperties($request->agent_id);
        return datatables()->of($properties)
            ->addColumn('actions' , function ($property) {
                return \View::make('agents.properties.partials.actions' , [
                    'property_id' => $property->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function tenantProperties(Request $request)
    {
        $properties = $this->propertyRepository->getTenantProperties($request->tenant_id);
        return datatables()->of($properties)
            ->addColumn('actions' , function ($property) {
                return \View::make('tenants.properties.partials.actions' , [
                    'property_id' => $property->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function agentTenants(Request $request)
    {
        $users = $this->userRepository->getAgentTenants($request->agent_id);
        return datatables()->of($users)
            ->addColumn('actions' , function ($user) {
                return \View::make('agents.users.partials.actions' , [
                    'user_id' => $user->id
                ])->render();
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function adminInvoices()
    {
        $invoices = $this->invoiceRepository->getAdminInvoices();

        return datatables()->of($invoices)
            ->editColumn('status' , function ($invoice) {
                return \View::make('invoice.partials.status_partial' , [
                    'invoice' => $invoice
                ])->render();
            })
            ->addColumn('actions' , function ($invoice) {
                return \View::make('invoice.partials.actions' , [
                    'invoice_id' => encodeId($invoice->id)
                ])->render();
            })
            ->addColumn('amount_due' , function ($invoice) {
                return 'R '.number_format($invoice->total_debit - $invoice->total_credit, 2);
            })
            ->rawColumns(['actions', 'status'])
            ->make(true);
    }
    public function agentInvoices(Request $request)
    {
        $invoices = $this->invoiceRepository->getAgentInvoices($request->agent_id);
        return datatables()->of($invoices)
            ->editColumn('status' , function ($invoice) {
                return \View::make('invoice.partials.status_partial' , [
                    'invoice' => $invoice
                ])->render();
            })
            ->addColumn('actions' , function ($invoice) {
                return \View::make('invoice.partials.actions' , [
                    'invoice_id' => encodeId($invoice->id)
                ])->render();
            })
            ->addColumn('amount_due' , function ($invoice) {
                return 'R '.number_format($invoice->total_debit - $invoice->total_credit, 2);
            })
            ->rawColumns(['actions', 'status'])
            ->make(true);
    }

    public function tenantInvoices(Request $request)
    {
        $invoices = $this->invoiceRepository->getTenantInvoices($request->tenant_id);
        return datatables()->of($invoices)
            ->editColumn('status' , function ($invoice) {
                return \View::make('invoice.partials.status_partial' , [
                    'invoice' => $invoice
                ])->render();
            })
            ->addColumn('actions' , function ($invoice) {
                return \View::make('tenants.invoices.partials.actions' , [
                    'invoice_id' => encodeId($invoice->id)
                ])->render();
            })
            ->addColumn('amount_due' , function ($invoice) {
                return 'R '.number_format($invoice->total_debit - $invoice->total_credit, 2);
            })
            ->rawColumns(['actions', 'status'])
            ->make(true);
    }
}
