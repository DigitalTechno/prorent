<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Vinkla\Hashids\Facades\Hashids;

class CustomAuthController extends Controller
{
    public function replaceAuth($user_id)
    {
        // If Admin
        if(Auth::user()->user_type_id == 1 || Auth::user()->user_type_id == 2){
            // If user exists
            $user_id = Hashids::decode($user_id);

            $user = User::where('id', $user_id)->get();
            if($user->count()){
                $user = User::where('id', $user_id)->first();
                // Log out as Admin
                Auth::logout();
                // Log in
                Auth::login($user);
                // Redirect to home
                return redirect()->route('home');
            }
            // If no user exists
            return redirect()->back()->with(['error' => 'No user!']);
        }
        // If not Admin
        return redirect()->back()->with(['error' => 'Not authorised!']);

    }
}
