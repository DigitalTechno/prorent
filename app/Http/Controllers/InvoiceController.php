<?php

namespace App\Http\Controllers;

use App\Repositories\Properties\PropertyRepository;
use App\Repositories\Users\UserRepository;
use Auth;
use App\Mail\InvoiceMail;
use App\Models\Invoice;
use PDF;
use App\Models\PaymentStatus;
use App\Models\Property;
use App\Models\Recurring;
use App\Models\RecurringCycle;
use App\Models\TaxType;
use App\Repositories\Invoices\InvoiceRepo;
use App\Services\Invoices\InvoiceService;
use App\User;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    private $invoiceService;
    private $invoiceRepository;
    private $userRepository;
    private $propertyRepository;

    public function __construct(
        InvoiceService $invoiceService,
        InvoiceRepo $invoiceRepository,
        UserRepository $userRepository,
        PropertyRepository $propertyRepository
    )
    {
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
        $this->userRepository = $userRepository;
        $this->propertyRepository = $propertyRepository;
    }

    public function admin_invoices()
    {
        $payment_statuses = PaymentStatus::all();
        $recurrings = Recurring::all();
        $recurring_cycles = RecurringCycle::all();
        $tenants = User::where('user_type_id', 3)->get();
        $properties = Property::limit(30)->get();
        $tax_types = TaxType::all();
        return view('admin.invoices.index', compact(
            'tenants',
            'recurring_cycles',
            'recurrings',
            'payment_statuses',
            'properties',
            'tax_types'
        ));
    }

    public function agent_invoices()
    {
        $user = Auth::user();
        $payment_statuses = PaymentStatus::all();
        $recurrings = Recurring::all();
        $recurring_cycles = RecurringCycle::all();
        $tenants = $this->userRepository->getAgentTenants($user->id)->get();
        $properties = $this->propertyRepository->getAgentProperties($user->id)->get();
        $tax_types = TaxType::all();
        return view('agents.invoices.index', compact(
            'tenants',
            'recurring_cycles',
            'recurrings',
            'payment_statuses',
            'properties',
            'tax_types'
        ));
    }

    public function tenant_invoices()
    {
        $user = Auth::user();
        $payment_statuses = PaymentStatus::all();
        $recurrings = Recurring::all();
        $recurring_cycles = RecurringCycle::all();
        $tenants = $this->userRepository->getAgentTenants($user->id)->get();
        $properties = $this->propertyRepository->getTenantProperties($user->id)->get();
        $tax_types = TaxType::all();
        return view('tenants.invoices.index', compact(
            'tenants',
            'recurring_cycles',
            'recurrings',
            'payment_statuses',
            'properties',
            'tax_types'
        ));
    }

    public function store(Request $request)
    {
        $invoice = $this->invoiceService->store($request);
        return response()->json([
            'success' => "Invoice saved successfully.",
            'invoiceId' => encodeId($invoice->id)
        ]);
    }

    public function info($invoiceId)
    {
        $invoice = $this->invoiceRepository->getInvoice(decodeId($invoiceId));
        $invoice->items = $this->invoiceRepository->getInvoiceItems(decodeId($invoiceId));
        $total_credit = 0;
        $total_debit = 0;
        foreach ($invoice->items as $invoice_item){
            $total_credit += $invoice_item->credit;
            $total_debit += $invoice_item->debit;
        }

        $invoice->total_due = $total_debit-$total_credit;
        $invoice->payfast = $this->invoiceService->payfastButton($invoice);
        return view('invoice.info', compact('invoice'));
    }

    public function download($invoiceId)
    {
        $invoice = $this->invoiceRepository->getInvoice(decodeId($invoiceId));
        $invoice->items = $this->invoiceRepository->getInvoiceItems(decodeId($invoiceId));
        $pdf = PDF::loadView('invoice.invoice_pdf', [
            'invoice' => $invoice
        ]);

        return $pdf->download($invoice->invoice_number.'.pdf');
    }

    public function update(Request $request, $invoiceId)
    {
        $invoice = $this->invoiceService->update($request, decodeId($invoiceId));
        return response()->json([
            'success' => "Invoice updated successfully.",
            'invoiceId' => encodeId($invoice->id)
        ]);
    }

    public function edit(Request $request, $invoiceId)
    {
        $payment_statuses = PaymentStatus::all();
        $recurrings = Recurring::all();
        $recurring_cycles = RecurringCycle::all();
        $tenants = User::where('user_type_id', 3)->get();
        $properties = Property::limit(30)->get();
        $tax_types = TaxType::all();
        $invoice = $this->invoiceRepository->getInvoice(decodeId($invoiceId));
        $invoice->items = $this->invoiceRepository->getInvoiceItems(decodeId($invoiceId));
        return view('invoice.update', compact(
            'tenants',
            'recurring_cycles',
            'recurrings',
            'payment_statuses',
            'properties',
            'tax_types',
            'invoice'
        ));
    }

    public function send($invoiceId)
    {
        $invoice = $this->invoiceRepository->getInvoice(decodeId($invoiceId));
        $invoice->items = $this->invoiceRepository->getInvoiceItems(decodeId($invoiceId));
        $pdf = PDF::loadView('invoice.invoice_pdf', [
            'invoice' => $invoice
        ]);
        //check if folder has files
        if(\File::exists('invoices/')) {
            \File::deleteDirectory('invoices/');
        }
        // Create folder
        \File::makeDirectory('invoices/', $mode = 0775, true, true);

        $pdf->save('invoices/'. $invoice->invoice_number.'.pdf');
        $invoice->path = '/invoices/' . $invoice->invoice_number .'.pdf';

        $tenant = User::find($invoice->tenant_id);
        $property = Property::find($invoice->property_id);
        \Mail::to($tenant->email)->send(new InvoiceMail($tenant, $invoice, $property));
        return redirect()->back();
    }

    public function delete($invoiceId)
    {
        Invoice::where('id', decodeId($invoiceId))->delete();
        return back();
    }

    public function addItem($invoiceId)
    {
       $this->invoiceRepository->addInvoiceItem($invoiceId);
       $invoice = $this->invoiceRepository->getInvoice($invoiceId);
       $invoice->items = $this->invoiceRepository->getInvoiceItems($invoiceId);

       $html = \View::make('invoice.partials.invoice_item', [
           'invoice' => $invoice
       ])->render();

       return response()->json([
           'html' => $html
       ]);
    }
}
