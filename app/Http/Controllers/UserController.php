<?php

namespace App\Http\Controllers;

use App\Repositories\Invoices\InvoiceRepo;
use Auth;
use App\User;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Services\Users\UserService;
use App\Repositories\Users\UserRepository;
use App\Repositories\Properties\PropertyRepository;

class UserController extends Controller
{
    /**
     * @var UserService
    */
    private $userService;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var InvoiceRepo
     */
        private $invoiceRepository;

    public function __construct(
        UserService $userService,
        UserRepository $userRepository,
        PropertyRepository $propertyRepository,
        InvoiceRepo $invoiceRepository
    )
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->propertyRepository = $propertyRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function admins()
    {
        return view('admin.users.admin.index');
    }

    public function agents()
    {
        return view('admin.users.agents.index');
    }

    public function tenants()
    {
        return view('admin.users.tenants.index');
    }

    public function agent_tenants()
    {
        $user = Auth::user();
        $properties = $this->propertyRepository->getAgentProperties($user->id)->get();
        return view('agents.users.index', compact('properties'));
    }

    public function store(Request $request)
    {
        $this->userService->storeUser($request);
        return response()->json([
            'success' => 'User saved successfully.',
            'refresh' => true
        ]);
    }

    public function update(Request $request, $encodedId)
    {
        $userId = decodeId($encodedId);
        $this->userService->updateUser($request, $userId);
        return response()->json([
            'success' => 'User profile updated successfully.',
            'refresh' => true
        ]);

    }

    public function info($encodedId)
    {
        $userId = decodeId($encodedId);
        $user = $this->userRepository->getUserById($userId);
        $provinces = Province::get();
        $properties = $this->propertyRepository->getAgentProperties($userId)->get();
        $invoices = $this->invoiceRepository->getAgentInvoices($userId)->get();
        if($user->user_type_id == 3)
        {
            $properties = $this->propertyRepository->getTenantProperties($userId)->get();
            $invoices = $this->invoiceRepository->getTenantInvoices($userId)->get();
        }
        return view('admin.users.info', compact(
            'user',
            'properties',
            'provinces',
            'invoices'
        ));
    }

    public function delete($encodedId)
    {
        $userId = decodeId($encodedId);
        $user = User::find($userId);
        $user->delete();
        return redirect()->back();
    }
}
