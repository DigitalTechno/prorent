<?php

function decodeId($encodedId) : int {
    $decodedData = Hashids::decode($encodedId);
    $id = array_shift($decodedData);
    if (!$id && $id !== 0) throw new \Exception("Invalid Encrypted ID [$encodedId]");
    return intval($id);
}

function encodeId($id) : string {
    return Hashids::encode($id);
}

function uploadFile($uploaded_file)
{
    $path = $uploaded_file->store('public/files');
    $file_path = storage_path('app/'.$path);

    $size = getimagesize($file_path);
    $file = new App\Models\File;
    $file->name = $uploaded_file->getClientOriginalName();
    $file->path = $uploaded_file->hashName('');
    $file->ext = pathinfo($file_path, PATHINFO_EXTENSION);
    $file->mime_type = $uploaded_file->getMimeType();
    $file->width = $size[0];
    $file->height = $size[1];
    $file->save();

    return $file;
}

?>
