<?php

namespace App\Mail;

use App\Models\Property;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    private $tenant;
    private $invoice;
    private $property;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $tenant, $invoice, Property $property)
    {
        $this->tenant = $tenant;
        $this->invoice = $invoice;
        $this->property = $property;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.invoices.invoice', [
            'invoice' => $this->invoice,
            'tenant' => $this->tenant,
            'property' => $this->property
        ])->attach(public_path().$this->invoice->path, [
            'as' => $this->invoice->invoice_number.'.pdf'
        ])->subject('Invoice');
    }
}
