<?php

namespace App\Services\Invoices;

use App\Models\TenantProperty;
use App\Services\Properties\PropertyService;
use Auth;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Repositories\Invoices\InvoiceRepo;
use Billow\Payfast;
use Illuminate\Http\Request;

class InvoiceService
{
    private $invoice;
    private $invoiceRepository;
    private $payfast;
    private $propertyService;

    public function __construct(
        Invoice $invoice,
        InvoiceRepo $invoiceRepository,
        Payfast $payfast,
        PropertyService $propertyService
    )
    {
        $this->invoice = $invoice;
        $this->invoiceRepository = $invoiceRepository;
        $this->payfast = $payfast;
        $this->propertyService = $propertyService;
    }

    /**
     * @param Request $request
     * @return mixed
     */

    public function store(Request $request)
    {
        $request->validate([
            'tenant' => 'required',
            'due_date' => 'required',
            'property' => 'required',
            'recurring' => 'required'
        ]);

        $this->propertyService->updateTenantProperty($request->property, $request->tenant);

        $invNo = 'INV'.str_pad($request->tenant.''.$request->property, 4, 0, STR_PAD_LEFT);

        $invoice = new $this->invoice;
        $invoice->invoice_number = $invNo;
        $invoice->tenant_id = $request->tenant;
        $invoice->property_id = $request->property;
        $invoice->due_date = $request->due_date;
        $invoice->recurring_id = $request->recurring;
        $invoice->recurring_cycle_id = $request->recurring_cycle;
        $invoice->payment_status_id = $request->payment_status;
//        $invoice->notes = $request->description;
//        $invoice->total = $total;
        $invoice->save();
        return $invoice;
    }

    public function update(Request $request, $invoiceId)
    {
        $request->validate([
            'tenant' => 'required',
            'due_date' => 'required',
            'property' => 'required',
            'recurring' => 'required',
        ]);

        $this->propertyService->updateTenantProperty($request->property, $request->tenant);

        $invoice = $this->invoice->find($invoiceId);
        $invoice->invoice_number = $request->invoice_number;
        $invoice->tenant_id = $request->tenant;
        $invoice->property_id = $request->property;
        $invoice->due_date = $request->due_date;
        $invoice->recurring_id = $request->recurring;
        $invoice->recurring_cycle_id = $request->recurring_cycle;
        $invoice->payment_status_id = $request->payment_status;
//        $invoice->total = $total;
        $invoice->save();

        $this->updateInvoiceItem($request);
        return $invoice;
    }

    public function updateInvoiceItem($request)
    {
        foreach ($request->item_id as $key => $item_id)
        {
            $item = InvoiceItem::find($item_id);
            $item->title = $request->title[$key];
            $item->ref = $request->ref[$key];
            $item->credit = $request->credit[$key];
            $item->debit = $request->debit[$key];
            $item->date = $request->date[$key];
            $item->save();
        }
    }

    public function payfastButton($invoice)
    {
        // Build up payment Paramaters.
        $this->payfast->setItem(\Config::get('app.name') . ' Invoice Payment', \Config::get('app.name') . ' Invoice Payment');
        $this->payfast->setAmount($invoice->total_due);
        $this->payfast->setMerchantReference($invoice->invoice_number);
        $this->payfast->setBuyer(Auth::user()->name, Auth::user()->surname, Auth::user()->email);

        // add class to payfast button
        $doc = new \DOMDocument();
        $mock = new \DOMDocument();
        $doc->loadHTML($this->payfast->paymentForm('Pay Now'));

        $body = $doc->getElementsByTagName('body')->item(0);

        foreach ($body->childNodes as $key => $child) {
            $mock->appendChild($mock->importNode($child, true));
        }
        $button = $mock->getElementsByTagName('button')[0];
        $button->setAttribute('class', 'payfast-btn');

        $payfast = $mock->saveHTML();
        return $payfast;
    }
}