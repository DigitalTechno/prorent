<?php

namespace App\Services\Properties;

use App\Models\TenantProperty;
use DB;
use Auth;
use App\Models\AgentProperty;
use App\Models\Property;
use Illuminate\Http\Request;

class PropertyService
{
    public function storeProperty($request)
    {
        $request->validate([
            'title' => 'required',
            'category' => 'required',
            'property_type' => 'required',
            'bedrooms' => 'required',
            'bathrooms' => 'required',
            'price' => 'required',
            'description' => 'required',
            'address' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $property = new Property();
            $property->title = $request->title;
            $property->category_id = $request->category;
            $property->property_type_id = $request->property_type;
            $property->bedrooms = $request->bedrooms;
            $property->bathrooms = $request->bathrooms;
            $property->price = $request->price;
            $property->description = $request->description;
            $property->address = $request->address;
            $property->save();

            $user = Auth::user();
            $agent_id =  $user->id;
            if($request->agent_id){
                $agent_id = $request->agent_id;
            }
            
            if($user->user_type_id == 2)
            {
                $agentProperty = new AgentProperty();
                $agentProperty->property_id = $property->id;
                $agentProperty->agent_id = $agent_id;
                $agentProperty->save();
            }
            DB::commit();

        }catch (\Exception $e){

            DB::rollback();

            return \Response::json(array(
                'error' => $e->getMessage(),
                'message' => 'Oops! Something went wrong.'
            ));

        }

    }

    public function update(Request $request, $encodedId)
    {
        $decodedId = decodeId($encodedId);
        $property = Property::find($decodedId);
        $property->title = $request->title;
        $property->category_id = $request->category_id;
        $property->property_type_id = $request->property_type_id;
        $property->bedrooms = $request->bedrooms;
        $property->bathrooms = $request->bathrooms;
        $property->price = $request->price;
        $property->description = $request->description;
        $property->address = $request->address;
        $property->province_id = $request->province_id;
        $property->save();

        if($request->tenant_id)
        {
            $tenantProperty = TenantProperty::find($request->tenant_id);
            if(!$tenantProperty){
                $tenantProperty = new TenantProperty();
            }
            $tenantProperty->property_id = $property->id;
            $tenantProperty->tenant_id = $request->tenant_id;
            $tenantProperty->save();
        }
    }

    public function updateTenantProperty($propertyId, $tenantId)
    {
        $tenant_property = TenantProperty::where('property_id', $propertyId)->first();
        if($tenant_property){
            $tenant_property->tenant_id = $tenantId;
            $tenant_property->save();
        }
    }
}