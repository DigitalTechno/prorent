<?php

namespace App\Services\Users;

use DB;
use Auth;
use App\User;
use App\Models\AgentUser;
use Illuminate\Http\Request;
use App\Models\TenantProperty;

class UserService
{
    public function storeUser(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);

        DB::beginTransaction();
        try {

            $user = new User();
            $user->name = $request->name;
            $user->user_type_id = $request->user_type_id;
            $user->surname = $request->surname;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            if ($request->file('profile'))
            {
                $file = uploadFile($request->file('profile'));
            }
            $user->avatar = isset($file) ? $file->id : null;
            $user->save();

            $agent_id = Auth::user()->id;
            if ($request->agent_id) {
                $agent_id = $request->agent_id;
            }


            if ($request->user_type_id == 3) {
                $agentUser = new AgentUser();
                $agentUser->user_id = $user->id;
                $agentUser->agent_id = $agent_id;
                $agentUser->save();

                $agentProperty = new TenantProperty();
                $agentProperty->property_id = $request->property_id;
                $agentProperty->tenant_id = $user->id;
                $agentProperty->save();
            }

            DB::commit();

        }catch (\Exception $e){

            DB::rollback();

            return \Response::json(array(
                'error' => $e->getMessage(),
                'message' => 'Oops! Something went wrong.'
            ));

        }
    }

    public function updateUser(Request $request, $userId)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'email' => 'email|required|unique:users,email,'.$userId,
        ]);

        $user = User::find($userId);
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->date_of_birth = $request->date_of_birth;
        $user->marital_status = $request->marital_status;
        $user->suburb = $request->suburb;
        $user->contact_number = $request->contact_number;
        $user->home_address = $request->home_address;
        $user->work_address = $request->work_address;
        $user->city = $request->city;
        $user->province_id = $request->province_id;
        $user->gender = $request->gender;
        if($request->hasFile('avatar'))
        {
            $file = uploadFile($request->file('avatar'));
            if($file) {
                $user->avatar = $file->id;
            }
        }
        $user->save();

    }

}