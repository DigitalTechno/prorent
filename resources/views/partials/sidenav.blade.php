<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel"></div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu {{Request::is('/') ? 'active' : ''}}">
                <a href="{{route('index')}}" class="{{Request::is('/') ? 'active' : ''}}">
                    <span class="pcoded-micon"><i class="feather icon-list"></i></span>
                    <span class="pcoded-mtext">Dashboard</span>
                </a>
            </li>
        </ul>

        @if(Auth::user()->user_type_id == 1 || Auth::user()->user_type_id == 2)
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu {{Request::is('users/*') ? 'active' : ''}}">
                    <a href="javascript:void(0)">
                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                        <span class="pcoded-mtext">Users</span>
                        <span class="pcoded-mcaret"></span>
                    </a>
                    <ul class="pcoded-submenu">
                        @if(Auth::user()->user_type_id == 1)
                            <li class="{{Request::is('users/admins') ? 'active' : ''}}">
                                <a href="{{route('admins')}}">
                                    <span class="pcoded-mtext">Admins</span>
                                </a>
                            </li>
                            <li class="{{Request::is('users/agents') ? 'active' : ''}}">
                                <a href="{{route('agents')}}">
                                    <span class="pcoded-mtext">Agents</span>
                                </a>
                            </li>
                            <li class="{{Request::is('users/tenants') ? 'active' : ''}}">
                                <a href="{{route('tenants')}}">
                                    <span class="pcoded-mtext">Tenants</span>
                                </a>
                            </li>
                        @elseif(Auth::user()->user_type_id == 2)
                            <li class="{{Request::is('agent/tenants') ? 'active' : ''}}">
                                <a href="{{route('agent.tenants')}}">
                                    <span class="pcoded-mtext">Tenants</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
        @endif
        <ul class="pcoded-item pcoded-left-item">
            @if(Auth::user()->user_type_id == 1)
                <li class="pcoded-hasmenu {{Request::is('properties/*') ? 'active' : ''}}">
                    <a href="{{route('properties.index')}}">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Properties</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu {{Request::is('invoices/*') ? 'active' : ''}}">
                    <a href="{{route('admin.invoices')}}">
                        <span class="pcoded-micon"><i class="feather icon-file-minus"></i></span>
                        <span class="pcoded-mtext">Invoices</span>
                    </a>
                </li>
            @elseif(Auth::user()->user_type_id == 2)
                <li class="pcoded-hasmenu {{Request::is('properties/*') ? 'active' : ''}}">
                    <a href="{{route('agent.properties')}}">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Properties</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu {{Request::is('invoices/*') ? 'active' : ''}}">
                    <a href="{{route('agent.invoices')}}">
                        <span class="pcoded-micon"><i class="feather icon-file-minus"></i></span>
                        <span class="pcoded-mtext">Invoices</span>
                    </a>
                </li>
            @elseif(Auth::user()->user_type_id == 3)
                <li class="pcoded-hasmenu {{Request::is('properties/*') ? 'active' : ''}}">
                    <a href="{{route('tenant.properties')}}">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Properties</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu {{Request::is('invoices/*') ? 'active' : ''}}">
                    <a href="{{route('tenant.invoices')}}">
                        <span class="pcoded-micon"><i class="feather icon-file-minus"></i></span>
                        <span class="pcoded-mtext">Invoices</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>