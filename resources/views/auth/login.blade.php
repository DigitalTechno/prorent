@extends('layouts.auth')

@section('content')
    <!-- Pre-loader start -->
    @include('partials.pre-loader')
    <!-- Pre-loader end -->
    <form id="msform" action="{{route('login')}}" method="POST">
        {{csrf_field()}}
        <fieldset>
            <img class="logo" src="assets\images\logo-blue.png" alt="logo.png">
            <h2 class="fs-title">Sign In</h2>
            <h3 class="fs-subtitle">Sign In to access your account</h3>
            <div class="form-group">
                <input type="email" name="email"  class="form-control" placeholder="Email"><br/>
                @if($errors->has('email'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control"  name="password" placeholder="Password"><br/>
                @if($errors->has('password'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <a href="{{route('register')}}" class="btn btn-inverse btn-outline-inverse">Register</a>
            <button type="submit" class="btn btn-primary">Login</button>
        </fieldset>
    </form>

@endsection
