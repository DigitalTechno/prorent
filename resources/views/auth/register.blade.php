@extends('layouts.auth')

@section('content')
    <!-- Pre-loader start -->
 @include('partials.pre-loader')
    <!-- Pre-loader end -->
    <form id="msform"action="{{route('register')}}" method="POST">
        {{csrf_field()}}
        <fieldset>
            <img class="logo" src="assets\images\logo-blue.png" alt="logo.png">
            <h2 class="fs-title">Sign up</h2>
            <h3 class="fs-subtitle">Let’s have a new beginning. Sign up as an Agent</h3>
            <div class="form-group">
                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Name">
                @if($errors->has('name'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('name') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="surname" value="{{old('surname')}}" placeholder="Surname">
                @if($errors->has('surname'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('surname') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Email">
                @if($errors->has('email'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password">
                @if($errors->has('password'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                @if($errors->has('password_confirmation'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('password_confirmation') }}</span>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="contact_number" value="{{old('contact_number')}}" placeholder="Contact Number">
                @if($errors->has('contact_number'))
                    <span class="error text-danger" style="font-size: 10px;">{{ $errors->first('contact_number') }}</span>
                @endif
            </div>
            <a href="{{route('login')}}" class="btn btn-inverse btn-outline-inverse">Login</a>
            <button type="submit" class="btn btn-primary">Register</button>
        </fieldset>
    </form>

@endsection
