<!DOCTYPE html>
<html>
<head>
    <link href="{{ asset('css/print.css') }}" rel="stylesheet">
</head>
<body>
<style>
    .td-relative {
        position: relative;
    }
    .pdf-print-image {
        position: relative;
        text-align: center;
        width: 80px;
    }

</style>
<table cellspacing="0" border="1" class="pdf-table table-break-inside" width="100%">
    <thead>
    <tr>
        <th class="text-left" colspan="3">
            <img style="margin-top: 5px;" src="{{ public_path('assets/images/logo-blue.png') }}">
            <p class="text-left" style="margin-top: 35px;">
                ProRental<br/>
                1226 Tafelsig Flats, Loevenstein, Bellville<br/>
                hello@prorental.com<br/>
                +27 735 478 3008
            </p>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr class="text-left">
        <td>
            <strong>TENANT INFORMATION :</strong>
        </td>
        <td>
            <strong>INVOICE INFORMATION :</strong>
        </td>
        <td>
            <strong>INVOICE NUMBER: {{$invoice->invoice_number}}</strong>
        </td>
    </tr>
    <tr class="text-center">
        <td>
            <h3 class="m-0">{{$invoice->tenant}}</h3>
            <p class="m-0 m-t-10">{{$invoice->home_address}}</p>
            <p class="m-0">{{$invoice->contact_number}}</p>
            <p>{{$invoice->email}}</p>
        </td>
        <td>
            <p>
                Date: {{$invoice->created_at}}
            </p>
            <p>Status:
                @if($invoice->payment_status_id == 1)
                    <span class="label label-danger">{{$invoice->status}}</span>
                @elseif($invoice->payment_status_id == 2)
                    <span class="label label-warning">{{$invoice->status}}</span>
                @elseif($invoice->payment_status_id == 3)
                    <span class="label label-success">{{$invoice->status}}</span>
                @elseif($invoice->payment_status_id == 4)
                    <span class="label label-danger">{{$invoice->status}}</span>
                @endif
            </p>
        </td>
        <td>
{{--            TOTAL DUE : <span>R{{number_format(($invoice->total-$invoice->amount_paid), 2)}}</span>--}}
        </td>
    </tr>
    </tbody>
</table>
<table cellspacing="0" border="1" class="pdf-table table-break-inside" width="100%">
    <tr class="text-left">
        <th>Date</th>
        <th>Description</th>
        <th>Ref</th>
        <th>Debit</th>
        <th>Credit</th>
        <th>Balance</th>
    </tr>
    <tbody>
    <?php
    $total_credit = 0;
    $total_debit = 0;
    $total = 0;
    ?>
    @foreach($invoice->items as $item)
        <tr>
            <td class="v-mid">{{$item->date}}</td>
            <td class="v-mid">{{$item->title}} </td>
            <td class="v-mid">{{$item->ref}} </td>
            <td class="v-mid">{{($item->debit > 0) ? 'R '.number_format($item->debit, 2) : '' }}</td>
            <td class="v-mid">{{($item->credit > 0) ? 'R '.number_format($item->credit, 2) : '' }}</td>
            <td class="v-mid"></td>
        </tr>
        @php
            $total_credit += $item->credit;
            $total_debit += $item->debit;
            $total = $total_debit-$total_credit;
        @endphp
    @endforeach
    <tr class="text-right">
        <td colspan="5">
            <strong>Amount due for payment ====> : </strong>
        </td>
        <td colspan="1">
            R {{number_format($total, 2)}}
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
