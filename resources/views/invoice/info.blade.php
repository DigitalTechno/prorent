
@extends('layouts.dashboard')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">

                    <!-- Page body start -->
                    <div class="page-body">
                        <!-- Container-fluid starts -->
                        <div class="container">
                            <!-- Main content starts -->
                            <div>
                                <!-- Invoice card end -->
                                <div class="row text-center">
                                    <div class="col-sm-12 invoice-btn-group text-right m-r-20">
                                        @if(Auth::user()->user_type_id != 3)
                                            <a href="{{route('invoice.edit', encodeId($invoice->id))}}" class="btn btn-primary text-uppercase text-white m-b-10 btn-sm waves-effect waves-light">Edit</a>
                                            <a href="{{route('invoice.send', encodeId($invoice->id))}}" class="btn btn-success text-uppercase waves-effect m-b-10 btn-sm waves-light text-white">Send</a>
                                        @endif
                                            <a href="{{route('invoice.download', encodeId($invoice->id))}}" class="btn btn-inverse text-uppercase waves-effect m-b-10 btn-sm waves-light text-white">Download</a>

                                    </div>
                                </div>
                                <!-- Invoice card start -->
                                <div class="card">
                                    <div class="row invoice-contact">
                                        <div class="col-md-8">
                                            <div class="invoice-box row">
                                                <div class="col-sm-12">
                                                    <table class="table table-responsive invoice-table table-borderless">
                                                        <tbody>
                                                        <tr>
                                                            <td><img src="{{asset('assets\images\logo-blue.png')}}" class="m-b-10" alt=""></td>
                                                        </tr>
                                                        <tr>
                                                            <td>ProRental</td>
                                                        </tr>
                                                        <tr>
                                                            <td>1226 Tafelsig Flats, Loevenstein, Bellville</td>
                                                        </tr>
                                                        <tr>
                                                            <td>hello@prorental.com</td>
                                                        </tr>
                                                        <tr>
                                                            <td>+27 735 478 3008</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="row invoive-info">
                                            <div class="col-md-4 col-xs-12 invoice-client-info">
                                                <h6>Tenant Information :</h6>
                                                <h6 class="m-0">{{$invoice->tenant}}</h6>
                                                <p class="m-0 m-t-10">{{$invoice->home_address}}</p>
                                                <p class="m-0">{{$invoice->contact_number}}</p>
                                                <p>{{$invoice->email}}</p>
                                            </div>
                                            <div class="col-md-4 col-sm-6">
                                                <h6>Invoice Information :</h6>
                                                <table class="table table-responsive invoice-table invoice-order table-borderless">
                                                    <tbody>
                                                    <tr>
                                                        <th>Date :</th>
                                                        <td>{{$invoice->created_at}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status :</th>
                                                        <td>
                                                            @if($invoice->payment_status_id == 1)
                                                                <span class="label label-danger">{{$invoice->status}}</span>
                                                            @elseif($invoice->payment_status_id == 2)
                                                                <span class="label label-warning">{{$invoice->status}}</span>
                                                            @elseif($invoice->payment_status_id == 3)
                                                                <span class="label label-success">{{$invoice->status}}</span>
                                                            @elseif($invoice->payment_status_id == 4)
                                                                <span class="label label-danger">{{$invoice->status}}</span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-4 col-sm-6">
                                                <h6 class="m-b-20">Invoice Number: <span>{{$invoice->invoice_number}}</span></h6>
                                                @if(Auth::user()->user_type_id == 3 && $invoice->total_due > 0)
                                                    {!! $invoice->payfast !!}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table invoice-detail-table">
                                                        <thead>
                                                        <tr class="thead-default">
                                                            <th>Date</th>
                                                            <th class="text-left">Description</th>
                                                            <th>Ref</th>
                                                            <th>Debit</th>
                                                            <th>Credit</th>
                                                            <th>Balance</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            $total_credit = 0;
                                                            $total_debit = 0;
                                                            $total = 0;
                                                        ?>
                                                        @foreach($invoice->items as $item)
                                                            <tr class="text-left">
                                                                <td>{{$item->date}}</td>
                                                                <td class="text-left">{{$item->title}} </td>
                                                                <td>{{$item->ref}} </td>
                                                                <td>{{($item->debit > 0) ? 'R '.number_format($item->debit, 2) : '' }}</td>
                                                                <td>{{($item->credit > 0) ? 'R '.number_format($item->credit, 2) : '' }}</td>
                                                                <td></td>
                                                            </tr>
                                                            @php
                                                                $total_credit += $item->credit;
                                                                $total_debit += $item->debit;
                                                                $total = $total_debit-$total_credit;
                                                            @endphp
                                                        @endforeach

                                                        <tr class="text-info">
                                                            <td colspan="5" class="text-right">
                                                                <hr>
                                                                <h5 class="text-primary">Amount due for payment ====> :</h5>
                                                                <hr>
                                                            </td>
                                                            <td colspan="1">
                                                                <hr>
                                                                <h5 class="text-primary">R {{number_format($total, 2)}}</h5>
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h6>Terms And Condition :</h6>
                                                <p>Terms </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Container ends -->
                    </div>
                    <!-- Page body end -->
                </div>
            </div>
            <!-- Warning Section Starts -->

            <div id="styleSelector">

            </div>
        </div>
    </div>
@endsection
