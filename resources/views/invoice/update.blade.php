@extends('layouts.dashboard')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Update Invoice</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{route('index')}}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('index')}}">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('invoice.info', encodeId($invoice->id))}}">Invoice</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->

                    <!-- Page-body start -->
                    <div class="page-body">
                        <!--profile cover end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- tab content start -->
                                <div class="tab-content">
                                    <!-- tab pane personal end -->
                                    <!-- tab pane info start -->
                                    <div class="tab-pane active" id="updateInfo" role="tabpanel">
                                        <!-- info card start -->
                                        <div class="card">

                                            <!-- end of view-info -->
                                            <div class="edit-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <form class="ajax-form" action="{{route('invoice.update', encodeId($invoice->id))}}" method="POST" enctype="multipart/form-data">
                                                            {{csrf_field()}}

                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Invoice Number</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" readonly name="invoice_number" value="{{$invoice->invoice_number}}" class="form-control" placeholder="Name/Title">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Tenant</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="tenant" class="form-control">
                                                                                                <option value="">---- Tenant ----</option>
                                                                                                @foreach($tenants as $tenant)
                                                                                                    <option value="{{$tenant->id}}" {{$invoice->tenant_id == $tenant->id ? 'selected' : ''}}>{{$tenant->name . ' ' . $tenant->surname}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Property</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="property" class="form-control">
                                                                                                <option value="">-- select property --</option>
                                                                                                @foreach($properties as $property)
                                                                                                    <option value="{{$property->id}}" {{$property->id == $invoice->property_id ? 'selected' : ''}}>{{$property->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Recurring</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="recurring" class="form-control">
                                                                                                <option value="">-- select recurring --</option>
                                                                                                @foreach($recurrings as $recurring)
                                                                                                    <option value="{{$recurring->id}}" {{$recurring->id == $invoice->recurring_id ? 'selected' : ''}}>{{$recurring->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Payment Status</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="payment_status" class="form-control">
                                                                                                <option value="">-- select payment status --</option>
                                                                                                @foreach($payment_statuses as $payment_status)
                                                                                                    <option value="{{$payment_status->id}}" {{$payment_status->id == $invoice->payment_status_id ? 'selected' : ''}}>{{$payment_status->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Due Date</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="date" name="due_date" value="{{$invoice->due_date}}" class="form-control" placeholder="">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Recurring Cycle</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="recurring_cycle" class="form-control">
                                                                                                <option value="">-- select recurring cycle--</option>
                                                                                                @foreach($recurring_cycles as $recurring)
                                                                                                    <option value="{{$recurring->id}}" {{$recurring->id == $invoice->recurring_cycle_id ? 'selected' : ''}}>{{$recurring->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <h5 class="ml-4">Invoice Items</h5>
                                                                        <div class="invoice-items">
                                                                            @include('invoice.partials.invoice_item')
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>

                                                                <div class="text-center">
                                                                    <a data-id="{{$invoice->id}}" class="text-center add-invoice-item btn btn-inverse btn-outline-inverse"  style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add Invoice Item" >
                                                                        <i class="icofont icofont-plus"></i>
                                                                    </a>
                                                                </div>
                                                                <!-- end of row -->
                                                                <div class="text-right mb-5">
                                                                    <button type="submit" class="btn btn-inverse waves-effect waves-light m-r-20">Save Changes</button>
                                                                </div>
                                                            </div>

                                                        </form>
                                                        <!-- end of edit info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- tab content end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page-body end -->
                </div>
            </div>
            <!-- Main body end -->
            <div id="styleSelector">

            </div>
        </div>
    </div>
@endsection
