<table class="table">
    <tr>
        <th class="text-center v-mid">
            Date
        </th>
        <th class="text-center v-mid">
            Description
        </th>
        <th class="text-center v-mid">
            Ref
        </th>
        <th class="text-center v-mid">
            Debit
        </th>
        <th class="text-center v-mid">
            Credit
        </th>
    </tr>
    <tbody>
    <div class="">
        @foreach($invoice->items as $item)
            <tr class="text-left">
                <input type="hidden" name="item_id[]" value="{{$item->id}}"/>
                <td class="v-mid">
                    <input type="date" name="date[]" value="{{$item->date}}" class="form-control">
                </td>
                <td class="v-mid">
                    <input type="text" name="title[]" value="{{$item->title}}" class="form-control">
                </td>
                <td class="v-mid">
                    <input type="text" name="ref[]" value="{{$item->ref}}" class="form-control">
                </td>
                <td class="v-mid">
                    <input type="text" name="debit[]" value="{{$item->debit}}" class="form-control">
                </td>
                <td class="v-mid">
                    <input type="text" name="credit[]" value="{{$item->credit}}" class="form-control">
                </td>
            </tr>
        @endforeach
    </div>
    </tbody>
</table>