@if($invoice->payment_status_id == 1)
    <span class="label label-danger">{{$invoice->status}}</span>
@elseif($invoice->payment_status_id == 2)
    <span class="label label-warning">{{$invoice->status}}</span>
@elseif($invoice->payment_status_id == 3)
    <span class="label label-success">{{$invoice->status}}</span>
@elseif($invoice->payment_status_id == 4)
    <span class="label label-danger">{{$invoice->status}}</span>
@endif