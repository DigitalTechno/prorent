@extends('layouts.dashboard')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Invoices</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{route('index')}}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('index')}}">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('admin.invoices')}}">Invoices</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->

                    <div class="page-body">
                        <!-- Server Side Processing table start -->
                        <div class="card">
                            <div class="card-header">
                                <button type="button" class="btn btn-inverse f-right" data-toggle="modal" data-target="#add-invoice"><i class="icofont icofont-plus m-r-5"></i>New Invoice</button>
                            </div>
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="agentInvoices" class="table table-striped table-bordered nowrap">
                                        <thead>
                                        <tr>
                                            <th>Invoice Number</th>
                                            <th>Status</th>
                                            <th>Tenant</th>
                                            <th>Due Date</th>
                                            <th>Amount Due</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>Invoice Number</th>
                                            <th>Status</th>
                                            <th>Tenant</th>
                                            <th>Due Date</th>
                                            <th>Amount Due</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Warning Section Starts -->
            <div id="styleSelector">

            </div>
        </div>
    </div>
    @include('modals.add_invoice')
@endsection
