<!-- Modal -->
<div class="modal fade modal-flex" id="add-invoice" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Invoice</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="ajax-form" action="{{route('store.invoice')}}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body text-center">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Basic Form Inputs card start -->
                                <div class="card">
                                    <div class="card-block">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Tenant</label>
                                            <div class="col-sm-10">
                                                <select name="tenant" class="form-control">
                                                    <option value="">--Select Tenant--</option>
                                                    @foreach($tenants as $tenant)
                                                        <option value="{{$tenant->id}}">{{$tenant->name .' '.$tenant->surname}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row hide">
                                            <label class="col-sm-2 col-form-label">Property</label>
                                            <div class="col-sm-10">
                                                <select name="property" class="form-control">
                                                    <option value="">--Select Property--</option>
                                                    @foreach($properties as $property)
                                                        <option value="{{$property->id}}">{{$property->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Due Date</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="due_date" class="form-control" placeholder="">
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Recurring</label>
                                            <div class="col-sm-10">
                                                <select name="recurring" class="form-control">
                                                    <option value="">--Select Recurring--</option>
                                                    @foreach($recurrings as $recurring)
                                                        <option value="{{$recurring->id}}">{{$recurring->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Recurring Cycle</label>
                                            <div class="col-sm-10">
                                                <select name="recurring_cycle" class="form-control">
                                                    <option value="">--Select Recurring--</option>
                                                    @foreach($recurring_cycles as $recurring)
                                                        <option value="{{$recurring->id}}">{{$recurring->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Payment Status</label>
                                            <div class="col-sm-10">
                                                <select name="payment_status" class="form-control">
                                                    <option value="">--Select Payment Status--</option>
                                                    @foreach($payment_statuses as $status)
                                                        <option value="{{$status->id}}">{{$status->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>