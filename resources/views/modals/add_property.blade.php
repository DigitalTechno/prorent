<!-- Modal -->
<div class="modal fade modal-flex" id="add-property" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create New Property</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="ajax-form" action="{{route('properties.store')}}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body text-center">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Basic Form Inputs card start -->
                                <div class="card">
                                    <div class="card-block">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Title</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="title" class="form-control" placeholder="">
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Category</label>
                                            <div class="col-sm-10">
                                                <select name="category" class="form-control">
                                                    <option value="">--Select Category--</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Property Type</label>
                                            <div class="col-sm-10">
                                                <select name="property_type" class="form-control">
                                                    <option value="">--Select For Sale/To Rent--</option>
                                                    @foreach($property_types as $type)
                                                        <option value="{{$type->id}}">{{$type->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Bedrooms</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="bedrooms" class="form-control" placeholder="">
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Bathrooms</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="bathrooms" class="form-control" placeholder="">
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Price</label>
                                            <div class="col-sm-10">
                                                <input type="number" name="price" class="form-control" placeholder="">
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea rows="5" cols="5" name="description" class="form-control" placeholder="Description"></textarea>
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Address</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="address" class="form-control" placeholder="">
                                                <span class="error text-danger text-lef"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>