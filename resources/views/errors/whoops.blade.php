<!DOCTYPE html>

<html lang="en-us" class="no-js">

<head>
    <meta charset="utf-8">
    <title>ProRental</title>
    <meta name="description" content="Flat able 404 Error page design">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Codedthemes">
    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets\images\favicon.png')}}" type="image/*">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/error.css')}}">
</head>

<body>

<div class="image"></div>

<!-- Your logo on the top left -->
<a href="{{route('index')}}" class="logo-link" title="back home">

    <img src="{{asset('assets\images\logo.png')}}" class="logo" alt="Company's logo">

</a>

<div class="content">

    <div class="content-box">

        <div class="big-content">

            <!-- Main squares for the content logo in the background -->
            <div class="list-square">
                <span class="square"></span>
                <span class="square"></span>
                <span class="square"></span>
            </div>

            <!-- Main lines for the content logo in the background -->
            <div class="list-line">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>

            <!-- The animated searching tool -->
            <i class="fa fa-search" aria-hidden="true"></i>

            <!-- div clearing the float -->
            <div class="clear"></div>

        </div>

        <!-- Your text -->
        <h1>Oops! Error 404 not found.</h1>

        <p>The page you were looking for doesn't exist.<br>
            We think the page may have moved.</p>
        <p class="text-inverse"> {{ 'Error on line '.$exception->getLine().' in '.$exception->getFile() }}
        <hr>
        {{ $exception->getMessage() }}</p>

    </div>

</div>

<footer class="light">

    <ul>
        <li><a href="{{route('index')}}">Home</a></li>
    </ul>

</footer>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

</body>

</html>