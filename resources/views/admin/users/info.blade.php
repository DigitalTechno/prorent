@extends('layouts.dashboard')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>{{$user->name}} Profile</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{route('index')}}"> <i class="feather icon-home"></i> Dashboard </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('users.info', Hashids::encode($user->id))}}">User Profile</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->

                    <!-- Page-body start -->
                    <div class="page-body">
                        <!--profile cover start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="cover-profile">
                                    <div class="profile-bg-img">
                                        <img class="profile-bg-img img-fluid" src="{{asset('assets\images\user-profile\bg-img1.jpg')}}" alt="bg-img">
                                        <div class="card-block user-info">
                                            <div class="col-md-12">
                                                <div class="media-left">
                                                    <a href="#" class="profile-image">
                                                        <img class="user-img img-radius" width="150px" src="{{isset($user->path) ? asset('storage/files/'.$user->path) : asset('assets\images\avatar-4.jpg')}}" alt="user-img">
                                                    </a>
                                                </div>
                                                <div class="media-body row">
                                                    <div class="col-lg-12">
                                                        <div class="user-title">
                                                            <h2>{{$user->name .' '.$user->surname}}</h2>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--profile cover end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- tab header start -->
                                <div class="tab-header card">
                                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#updateInfo" role="tab">Update Profile</a>
                                            <div class="slide"></div>
                                        </li>
                                        @if($user->user_type_id != 1)
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#properties" role="tab">Properties</a>
                                                <div class="slide"></div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#invoices" role="tab">Invoices</a>
                                                <div class="slide"></div>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                <!-- tab header end -->
                                <!-- tab content start -->
                                <div class="tab-content">
                                    <!-- tab panel personal start -->
                                    <div class="tab-pane active" id="personal" role="tabpanel">
                                        <!-- personal card start -->
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-header-text">{{$user->name . ' '.$user->surname}}'s Profile</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="view-info">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-xl-6">
                                                                        <div class="table-responsive">
                                                                            <table class="table m-0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th scope="row">Full Name</th>
                                                                                    <td>{{$user->name . ' '.$user->surname}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Gender</th>
                                                                                    <td>{{$user->gender == 1 ? 'Male' : ''}} {{$user->gender == 2 ? 'Female' : ''}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Birth Date</th>
                                                                                    <td>{{$user->date_of_birth}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Marital Status</th>
                                                                                    <td>{{$user->marital_status}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Suburb</th>
                                                                                    <td>{{$user->suburb}}</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-12 col-xl-6">
                                                                        <div class="table-responsive">
                                                                            <table class="table">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th scope="row">Email</th>
                                                                                    <td><a href="#!"><span class="__cf_email__" data-cfemail="4206272f2d02273a232f322e276c212d2f">{{$user->email}}</span></a></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Mobile Number</th>
                                                                                    <td>{{$user->contact_number}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Home Address</th>
                                                                                    <td>{{$user->home_address}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Work Address</th>
                                                                                    <td>{{$user->work_address}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">City</th>
                                                                                    <td>{{$user->city}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Province</th>
                                                                                    <td>{{$user->province}}</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>
                                                            <!-- end of general info -->
                                                        </div>
                                                        <!-- end of col-lg-12 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of edit-info -->
                                            </div>
                                            <!-- end of card-block -->
                                        </div>
                                    </div>
                                    <!-- tab pane personal end -->
                                    <!-- tab pane properties start -->
                                    <div class="tab-pane" id="updateInfo" role="tabpanel">
                                        <!-- info card start -->
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-header-text">Update Profile</h5>
                                            </div>

                                            <!-- end of view-info -->
                                            <div class="edit-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <form class="ajax-form" action="{{route('users.update', encodeId($user->id))}}" method="POST" enctype="multipart/form-data">
                                                            {{csrf_field()}}

                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Name</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="name" value="{{$user->name}}" class="form-control" placeholder="Name">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Surname</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="surname" value="{{$user->surname}}" class="form-control" placeholder="Surname">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Date of Birth</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="date" id="datepicker" value="{{$user->date_of_birth}}" name="date_of_birth" class="form-control" placeholder="Select Your Birth Date">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Marital Status</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="marital_status" class="form-control js-example-basic-single">
                                                                                                <option value="">---- Marital Status ----</option>
                                                                                                <option value="married" {{$user->marital_status == 'married' ? 'selected' : ''}}>Married</option>
                                                                                                <option value="unmarried" {{$user->marital_status == 'unmarried' ? 'selected' : ''}}>Unmarried</option>
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Suburb</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="suburb" value="{{$user->suburb}}" class="form-control" placeholder="Suburb">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <div class="form-radio">
                                                                                            <div class="group-add-on">
                                                                                                <div class="radio radiofill radio-inline">
                                                                                                    <label>
                                                                                                        <input type="radio" name="gender" value="1" {{$user->gender == 1 ? 'checked' : ''}}=""><i class="helper"></i> Male
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="radio radiofill radio-inline">
                                                                                                    <label>
                                                                                                        <input type="radio" name="gender" value="2" {{$user->gender == 2 ? 'checked' : ''}}><i class="helper"></i> Female
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Upload Profile</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="file" data-id="{{$user->id}}" name="avatar" accept="image/*" class="form-control">
                                                                                            <span class="error text-danger text-left"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Email</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="email" name="email" value="{{$user->email}}" class="form-control" placeholder="Type Email Address">
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Contact Number</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="contact_number" value="{{$user->contact_number}}" class="form-control" placeholder="000 000 0000">
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Home Address</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="home_address" value="{{$user->home_address}}" class="form-control" placeholder="Address">
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Work Address</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="work_address" value="{{$user->work_address}}" class="form-control" placeholder="Address">
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">City</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="city" value="{{$user->city}}" class="form-control" placeholder="City">
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Province</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="province_id" class="form-control">
                                                                                                <option value="">---- Province ----</option>
                                                                                                @foreach($provinces as $province)
                                                                                                    <option value="{{$province->id}}" {{$user->province_id == $province->id ? 'selected' : ''}}>{{$province->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>
                                                                <!-- end of row -->
                                                                <div class="text-right mb-5 m-r-50">
                                                                    <button type="submit" class="btn btn-inverse waves-effect waves-light m-r-20">Save Changes</button>
                                                                </div>
                                                            </div>
                                                        {!! Form::close() !!}
                                                        <!-- end of edit info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- tab pane invoices start -->
                                    <div class="tab-pane" id="invoices" role="tabpanel">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <!-- contact data table card start -->
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h5 class="card-header-text">Invoices</h5>
                                                            </div>
                                                            <div class="card-block contact-details">
                                                                <div class="data_table_main table-responsive dt-responsive">
                                                                    <table class="table table-striped table-bordered nowrap">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Invoice Number</th>
                                                                            <th>Status</th>
                                                                            <th>Due Date</th>
                                                                            <th>Amount Due</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($invoices as $invoice)
                                                                            <tr>
                                                                                <td>{{$invoice->invoice_number}}</td>
                                                                                <td>
                                                                                    @include('invoice.partials.status_partial')
                                                                                </td>
                                                                                <td>{{$invoice->due_date}}</td>
                                                                                <td>R {{number_format($invoice->total_debit - $invoice->total_credit, 2)}}</td>
                                                                                <td>
                                                                                    <a href="{{route('invoice.info', encodeId($invoice->id))}}" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="icofont icofont-info-circle text-primary"></i></a>
                                                                                    @if(Auth::user()->user_type_id != 3)
                                                                                        <a href="{{route('invoice.send', encodeId($invoice->id))}}" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send"><i class="icofont icofont-email text-success"></i></a>
                                                                                    @endif
                                                                                    <a href="{{route('invoice.download', encodeId($invoice->id))}}" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download"><i class="icofont icofont-download text-inverse"></i></a>
                                                                                    @if(Auth::user()->user_type_id != 3)
                                                                                        <a href="{{route('invoice.delete', encodeId($invoice->id))}}" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="icofont icofont-delete-alt text-danger"></i></a>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- contact data table card end -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- tab pane contact end -->
                                    <div class="tab-pane" id="properties" role="tabpanel">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <!-- contact data table card start -->
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h5 class="card-header-text">Properties</h5>
                                                            </div>
                                                            <div class="card-block contact-details">
                                                                <div class="data_table_main table-responsive dt-responsive">
                                                                    <table id="simpletable" class="table  table-striped table-bordered nowrap">
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Title</th>
                                                                            <th>Category</th>
                                                                            <th>Property Type</th>
                                                                            <th>Description</th>
                                                                            <th>Price</th>
                                                                            <th>Date</th>
                                                                            <th>Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($properties as $property)
                                                                            <tr>
                                                                                <td>{{$property->title}}</td>
                                                                                <td>{{$property->category}}</td>
                                                                                <td>{{$property->property_type}}</td>
                                                                                <td>{{$property->description}}</td>
                                                                                <td>{{$property->price}}</td>
                                                                                <td>{{$property->created_at}}</td>
                                                                                <td>
                                                                                    <a href="{{route('properties.info', Hashids::encode($property->id))}}" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="icofont icofont-info-circle text-primary"></i></a>
                                                                                    @if(Auth::user()->user_type_id != 3)
                                                                                        <a href="{{route('properties.delete', Hashids::encode($property->id))}}" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="icofont icofont-delete-alt text-danger"></i></a>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- contact data table card end -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- tab content end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page-body end -->
                </div>
            </div>
            <!-- Main body end -->
            <div id="styleSelector">

            </div>
        </div>
    </div>
@endsection
