<a href="{{route('users.info', encodeId($user_id))}}" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="icofont icofont-info-circle text-primary"></i></a>
<a href="{{route('custom.login', encodeId($user_id))}}" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Login"><i class="feather icon-log-in text-inverse"></i></a>
<a href="{{route('users.delete', encodeId($user_id))}}" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="icofont icofont-delete-alt text-danger"></i></a>

