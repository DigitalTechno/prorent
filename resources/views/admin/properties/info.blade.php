@extends('layouts.dashboard')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Property Details</h4>
                                        </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{route('index')}}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('index')}}">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{route('properties.info', encodeId($property->id))}}">Property Details</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->

                    <!-- Page-body start -->
                    <div class="page-body">
                        <!--profile cover end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- tab header start -->
                                <div class="tab-header card">
                                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#details" role="tab">Property Info</a>
                                            <div class="slide"></div>
                                        </li>
                                        @if(Auth::user()->user_type_id != 3)
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#updateInfo" role="tab">Update Property</a>
                                                <div class="slide"></div>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                <!-- tab header end -->
                                <!-- tab content start -->
                                <div class="tab-content">
                                    <!-- tab panel personal start -->
                                    <div class="tab-pane active" id="details" role="tabpanel">
                                        <!-- personal card start -->
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="view-info">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-xl-6">
                                                                        <div class="table-responsive">
                                                                            <table class="table m-0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th scope="row">Name</th>
                                                                                    <td>{{$property->title}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Category</th>
                                                                                    <td>{{$property->category}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Property Type</th>
                                                                                    <td>{{$property->property_type}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Price</th>
                                                                                    <td>R {{number_format($property->price, 2)}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Property Address</th>
                                                                                    <td>{{$property->address}}</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-12 col-xl-6">
                                                                        <div class="table-responsive">
                                                                            <table class="table">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th scope="row">Tenant</th>
                                                                                    <td>{{$property->tenant}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Bedrooms</th>
                                                                                    <td>{{$property->bedrooms}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Bathrooms</th>
                                                                                    <td>{{$property->bathrooms}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Description</th>
                                                                                    <td>{{$property->description}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row">Province</th>
                                                                                    <td>{{$property->province}}</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>
                                                                <!-- end of row -->
                                                            </div>
                                                            <!-- end of general info -->
                                                        </div>
                                                        <!-- end of col-lg-12 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of view-info -->
                                            </div>
                                            <!-- end of card-block -->
                                        </div>
                                    </div>
                                    <!-- tab pane personal end -->
                                    <!-- tab pane info start -->
                                    <div class="tab-pane" id="updateInfo" role="tabpanel">
                                        <!-- info card start -->
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-header-text">Update Property</h5>
                                            </div>

                                            <!-- end of view-info -->
                                            <div class="edit-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <form class="ajax-form" action="{{route('properties.update', encodeId($property->id))}}" method="POST" enctype="multipart/form-data">
                                                            {{csrf_field()}}

                                                            <div class="general-info">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Name/Title</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="title" value="{{$property->title}}" class="form-control" placeholder="Name/Title">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Price</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="price" value="{{$property->price}}" class="form-control" placeholder="Price">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Bedrooms</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="number" value="{{$property->bedrooms}}" name="bedrooms" class="form-control" placeholder="Bedrooms">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Bathrooms</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="number" value="{{$property->bathrooms}}" name="bathrooms" class="form-control" placeholder="Bedrooms">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Property Address</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="address" value="{{$property->address}}" class="form-control" placeholder="Address">
                                                                                            <span class="error text-danger" style="font-size: 10px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                    <div class="col-lg-6">
                                                                        <table class="table">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Tenant</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="tenant_id" class="form-control">
                                                                                                <option value="">---- Tenant ----</option>
                                                                                                @foreach($tenants as $tenant)
                                                                                                    <option value="{{$tenant->id}}" {{$property->tenant_id == $tenant->id ? 'selected' : ''}}>{{$tenant->name . ' ' . $tenant->surname}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Description</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input type="text" name="description" value="{{$property->description}}" class="form-control" placeholder="Description">
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Property Type</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="property_type_id" class="form-control js-example-basic-single">
                                                                                                <option value="">---Property type---</option>
                                                                                                @foreach($property_types as $property_type)
                                                                                                    <option value="{{$property_type->id}}" {{$property->property_type_id == $property_type->id ? 'selected' : ''}}>
                                                                                                        {{$property_type->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Category</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="category_id" class="form-control">
                                                                                                <option value="">--- Category ---</option>
                                                                                                @foreach($categories as $category)
                                                                                                    <option value="{{$category->id}}" {{$property->category_id == $category->id ? 'selected' : ''}}>{{$category->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="form-group">
                                                                                        <label class="col-sm-2 col-form-label">Province</label>
                                                                                        <div class="col-sm-10">
                                                                                            <select name="province_id" class="form-control">
                                                                                                <option value="">---- Province ----</option>
                                                                                                @foreach($provinces as $province)
                                                                                                    <option value="{{$province->id}}" {{$property->province_id == $province->id ? 'selected' : ''}}>{{$province->title}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                            <span class="error text-danger text-lef"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!-- end of table col-lg-6 -->
                                                                </div>
                                                                <!-- end of row -->
                                                                <div class="text-right mb-5 m-r-50">
                                                                    <button type="submit" class="btn btn-inverse waves-effect waves-light m-r-20">Save Changes</button>
                                                                </div>
                                                            </div>
                                                        {!! Form::close() !!}
                                                        <!-- end of edit info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- tab content end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page-body end -->
                </div>
            </div>
            <!-- Main body end -->
            <div id="styleSelector">

            </div>
        </div>
    </div>
@endsection
