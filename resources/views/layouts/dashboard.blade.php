<!DOCTYPE html>
<html lang="en">

<head>
    <title>ProRental</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Digital Techno">
    <meta name="keywords" content="Digital Techno Property management system">
    <meta name="author" content="Digital Techno">
    <meta name="_token" content="{{ csrf_token() }}" />
    <!-- Favicon icon -->

    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/*">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components\bootstrap\css\bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\icon\themify-icons\themify-icons.css')}}">
    <!-- animation css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components\animate.css\css\animate.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\icon\icofont\css\icofont.css')}}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\icon\feather\css\feather.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\pages\flag-icon\flag-icon.min.css')}}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\pages\menu-search\css\component.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\jquery.mCustomScrollbar.css')}}">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\pages\data-table\css\buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css')}}">
    <link href="https://unpkg.com/pnotify@4.0.0/dist/PNotifyBrightTheme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css\styles.css')}}">

</head>

<body>
@include('partials.pre-loader')
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        @include('partials.navbar')
        @include('partials.side-chat')

        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                @include('partials.sidenav')
                @yield('content')
                <button type="button" class="btn btn-success btn-sm" id="pnotify-success"><i class="icofont icofont-play-alt-2"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="{{asset('bower_components\jquery\js\jquery.min.js')}}"></script>

<script type="text/javascript" src="{{asset('bower_components\jquery-ui\js\jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\popper.js\js\popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\bootstrap\js\bootstrap.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{asset('bower_components\jquery-slimscroll\js\jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{asset('bower_components\modernizr\js\modernizr.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\modernizr\js\css-scrollbars.js')}}"></script>


<!-- i18next.min.js -->
<script type="text/javascript" src="{{asset('bower_components\i18next\js\i18next.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\jquery-i18next\js\jquery-i18next.min.js')}}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{asset('assets\js\animation.js')}}"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="{{asset('bower_components\i18next\js\i18next.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components\jquery-i18next\js\jquery-i18next.min.js')}}"></script>
<!-- Custom js -->
<!-- sweet alert js -->
<script type="text/javascript" src="{{asset('bower_components\sweetalert\js\sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\modal.js')}}"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="{{asset('files\assets\js\modalEffects.js')}}"></script>
<script type="text/javascript" src="{{asset('files\assets\js\classie.js')}}"></script>
<!-- data-table js -->
<script src="{{asset('bower_components\datatables.net\js\jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components\datatables.net-buttons\js\dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets\pages\data-table\js\jszip.min.js')}}"></script>
<script src="{{asset('assets\pages\data-table\js\pdfmake.min.js')}}"></script>
<script src="{{asset('assets\pages\data-table\js\vfs_fonts.js')}}"></script>
<script src="{{asset('bower_components\datatables.net-buttons\js\buttons.print.min.js')}}"></script>
<script src="{{asset('bower_components\datatables.net-buttons\js\buttons.html5.min.js')}}"></script>
<script src="{{asset('bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('bower_components\datatables.net-responsive\js\dataTables.responsive.min.js')}}"></script>
<script src="{{asset('bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js')}}"></script>

<script src="{{asset('assets\js\pcoded.min.js')}}"></script>
<script src="{{asset('assets\js\vartical-layout.min.js')}}"></script>
<script src="{{asset('assets\js\jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\script.js')}}"></script>
<script type="text/javascript" src="{{asset('js\dashboard.js')}}"></script>
<script type="text/javascript" src="{{asset('js\ajax_datatables.js')}}"></script>
<script src="https://unpkg.com/pnotify@4.0.0/dist/umd/PNotify.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
</body>
</html>
