@component('mail::message')
#Dear {{$tenant->name . ' '. $tenant->surname}}

Property: {{$property->title}}

Property Address: {{$property->address}}

Attached please find invoice number <strong>{{$invoice->invoice_number}}</strong> for the above mentioned property.

Kind regards,

{{ config('app.name') }}
@endcomponent
